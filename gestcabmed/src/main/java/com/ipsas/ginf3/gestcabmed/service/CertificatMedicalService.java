package com.ipsas.ginf3.gestcabmed.service;

import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatAptitude;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatAptitudeResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatDispense;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatDispenseResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatRepos;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatReposResponse;

public interface CertificatMedicalService {
    CreateCertificatAptitudeResponse createCertificatAptitude(CreateCertificatAptitude createCertificatAptitude);
    CreateCertificatDispenseResponse createCertificatDispense(CreateCertificatDispense createCertificatDispense);
    CreateCertificatReposResponse createCertificatRepos(CreateCertificatRepos createCertificatRepos);
}
