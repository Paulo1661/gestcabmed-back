package com.ipsas.ginf3.gestcabmed.dao.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ipsas.ginf3.gestcabmed.dao.entity.Patient;
import com.ipsas.ginf3.gestcabmed.dao.entity.UtilisateurEntity;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {
	Patient findByPatientId(String patientId);
	Patient findByUtilisateurEntity(UtilisateurEntity utilisateur);
}
