package com.ipsas.ginf3.gestcabmed.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Data @AllArgsConstructor @NoArgsConstructor @ToString 
@Entity @Table(name = "resumes_consultations")
public class ResumeConsultationEntity {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "resume_consultation_id")
    private long id;
    private Date dateConsultation;
    private String resume;
    @Column(name = "resume_consultation_id_public", unique = true)
    private String resumeConsultationId;

    @OneToOne(mappedBy = "resumeConsultation")
    private Examen examen;

}
