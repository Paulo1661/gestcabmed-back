package com.ipsas.ginf3.gestcabmed.ui.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class CreateCertificatAptitudeResponse {
    private long dossierMedicalId;
    private boolean estApte;
    private String certificatAptitudeId;
}
