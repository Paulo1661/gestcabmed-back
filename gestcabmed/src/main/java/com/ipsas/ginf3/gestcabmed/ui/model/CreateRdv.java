package com.ipsas.ginf3.gestcabmed.ui.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import com.ipsas.ginf3.gestcabmed.dao.entity.Patient;
import com.ipsas.ginf3.gestcabmed.dao.entity.Rdv;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class CreateRdv {
	private String nom;
	private String prenom;
	private Date date;
	private String etat;
}
