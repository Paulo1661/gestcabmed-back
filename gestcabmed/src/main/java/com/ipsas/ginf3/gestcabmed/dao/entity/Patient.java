package com.ipsas.ginf3.gestcabmed.dao.entity;

import java.util.List;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
@Entity
public class Patient{
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private char sexe;
	private int codeCNAM;
	private int validite;
	@OneToOne
	private DossierMedicalEntity dossierMedical;
	@OneToMany
	private List<Rdv> list_rdv;
	@OneToOne
	private UtilisateurEntity utilisateurEntity;
	private String patientId;
}
