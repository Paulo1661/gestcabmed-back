package com.ipsas.ginf3.gestcabmed.service;

import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ipsas.ginf3.gestcabmed.dao.entity.LettreConfrereEntity;
import com.ipsas.ginf3.gestcabmed.dao.repository.ExamenRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.LettreConfrereRepository;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateLettreConfrereModel;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateLettreConfrereResponse;

@Service
public class LettreConfrereServiceImpl implements LettreConfrereService {
	private final LettreConfrereRepository lettreConfrereRepository;
	private final ExamenRepository examenRepository;
	@Autowired
	public LettreConfrereServiceImpl(LettreConfrereRepository lettreConfrereRepository, ExamenRepository examenRepository) {
		this.lettreConfrereRepository = lettreConfrereRepository;
		this.examenRepository = examenRepository;
		
	}

	@Override
	public CreateLettreConfrereResponse createLettreConfrere(CreateLettreConfrereModel lettre) {
		ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        LettreConfrereEntity lettreConfrereEntity = modelMapper.map(lettre, LettreConfrereEntity.class);
        lettreConfrereEntity.setLettreConfrereId(UUID.randomUUID()+"");
        lettreConfrereEntity.setExamen(examenRepository.findByExamenId(lettre.getExamenId()));
        lettreConfrereEntity = lettreConfrereRepository.save(lettreConfrereEntity);
        CreateLettreConfrereResponse lettreConfrereResponse = modelMapper.map(lettreConfrereEntity, CreateLettreConfrereResponse.class);
        return lettreConfrereResponse;
	}

}
