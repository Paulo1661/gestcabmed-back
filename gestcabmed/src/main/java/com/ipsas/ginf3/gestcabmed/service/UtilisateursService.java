package com.ipsas.ginf3.gestcabmed.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.ipsas.ginf3.gestcabmed.ui.dto.UtilisateurDto;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateUserResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.PatientModelResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.UpdateUserModel;

public interface UtilisateursService extends UserDetailsService {
    UtilisateurDto createUtilisateur(UtilisateurDto utilisateur);
    UtilisateurDto getUserDetailsByEmail(String userName);
    CreateUserResponse updateUtilisateur(UpdateUserModel updateUserModel);
	List<CreateUserResponse> getAllUtilisateurs();

}
