package com.ipsas.ginf3.gestcabmed.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data @NoArgsConstructor @AllArgsConstructor @ToString
@Entity @Table(name = "certificats_repos")
public class CertificatReposEntity extends CertificatMedical {
    @Column(name = "certificat_repos_id")
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "nombre_jour_de_repos", nullable = false, length = 4)
    private short nombreJoursDeRepos;
    @Column(name = "certificat_repos_id_public", unique = true)
    private String certificatReposId;
}
