package com.ipsas.ginf3.gestcabmed.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Embeddable
public class DetailOrdonnanceId implements Serializable {

    private long ordonnanceId;

    private long medicamentId;
}
