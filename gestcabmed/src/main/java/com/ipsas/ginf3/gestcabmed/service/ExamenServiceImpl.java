package com.ipsas.ginf3.gestcabmed.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ipsas.ginf3.gestcabmed.dao.entity.Examen;
import com.ipsas.ginf3.gestcabmed.dao.entity.LettreConfrereEntity;
import com.ipsas.ginf3.gestcabmed.dao.entity.Patient;
import com.ipsas.ginf3.gestcabmed.dao.repository.DossierMedicalRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.ExamenRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.PatientRepository;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateExamenModel;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateExamenResponseModel;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateLettreConfrereResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.ExamenResponse;
@Service
public class ExamenServiceImpl implements ExamenService {
	private final ExamenRepository examenRepository;
	private final DossierMedicalRepository dossierMedicalRepository;
	private final PatientRepository patientRepository;
	@Autowired
	public ExamenServiceImpl(ExamenRepository examenRepository, PatientRepository patientRepository, DossierMedicalRepository dossierMedicalRepository) {
		super();
		this.examenRepository = examenRepository;
		this.dossierMedicalRepository = dossierMedicalRepository;
		this.patientRepository = patientRepository;
	}

	@Override
	public List<Examen> getListExamen(String patientId) {
		Patient p = patientRepository.findByPatientId(patientId);
		Set<Examen> examens = dossierMedicalRepository.findByDossierMedicalId(p.getDossierMedical().getDossierMedicalId()).getList_Examen();
		List<Examen> examensResponse = new ArrayList<Examen>();
		for(Examen ex : examens) {
			examensResponse.add(ex);
		}
		
		return  examensResponse;
	}

	@Override
	public CreateExamenResponseModel createExamen(CreateExamenModel examen) {
		ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        Examen ex = modelMapper.map(examen, Examen.class);
        ex.setExamenId(UUID.randomUUID().toString());
        ex = examenRepository.save(ex);
        CreateExamenResponseModel examenResponse = modelMapper.map(ex, CreateExamenResponseModel.class);
        return examenResponse;
	}

}
