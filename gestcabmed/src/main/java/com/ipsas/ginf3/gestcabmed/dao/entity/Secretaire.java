package com.ipsas.ginf3.gestcabmed.dao.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Data @AllArgsConstructor @NoArgsConstructor @ToString

@Entity
public class Secretaire extends Personne{
	@Id
    @Column(name = "secretaire_id")
	private long id;

	@OneToOne
	private UtilisateurEntity utilisateurEntity;
}
