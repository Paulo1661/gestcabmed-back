package com.ipsas.ginf3.gestcabmed.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString

@Entity@Data
@AllArgsConstructor @NoArgsConstructor
public class Rdv {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String nom;
	private String prenom;
	private Date date;
	private String etat;
    @ManyToOne
    private Patient patient;
    @Column(unique = true)
    private String rdvId;
}
