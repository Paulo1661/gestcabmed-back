package com.ipsas.ginf3.gestcabmed.dao.repository;

import com.ipsas.ginf3.gestcabmed.dao.entity.OrdonnanceEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface OrdonnanceRepository extends CrudRepository<OrdonnanceEntity, Long> {
}
