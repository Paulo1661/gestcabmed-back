package com.ipsas.ginf3.gestcabmed.ui.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class CreateRdvResponse {
	private String nom;
	private String prenom;
	private Date date;
	private String etat;
	private String rdvId;
}
