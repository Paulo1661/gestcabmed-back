package com.ipsas.ginf3.gestcabmed.ui.model;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreatePatientResponse {

	private char sexe;
	private int codeCNAM;
	private int validite;
	private String patientId;
}
