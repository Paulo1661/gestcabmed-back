package com.ipsas.ginf3.gestcabmed.ui.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data @AllArgsConstructor @NoArgsConstructor @ToString

public class CreateCertificatDispenseResponse {
    private long dossierMedicalId;
	private Date dateDebut;
	private Date dateFin;
    private String certificatDispenseId;
}
