package com.ipsas.ginf3.gestcabmed.ui.model;


import com.ipsas.ginf3.gestcabmed.shared.ExamenComplementaire;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data @AllArgsConstructor @NoArgsConstructor @ToString

public class CreateExamenComplementaireEntityResponse {
    private long dossierMedicalId;
	private ExamenComplementaire examenComplementaire;
	private String examenComplementaireId;
}
