package com.ipsas.ginf3.gestcabmed.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
@Entity @Table(name = "lettres_confrere")
public class LettreConfrereEntity {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "lettre_confrere_id")
    private long id;
    private String emailConfrere;
    @Column(nullable = false, length = 40)
    private String objet;
    @Column(length = 800, nullable = false)
    private String contenu;
    @Column(name = "lettre_confrere_id_public", unique = true)
    private String lettreConfrereId;

    @OneToOne(mappedBy = "lettresConfrere")
    private Examen examen;
}
