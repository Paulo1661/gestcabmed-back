package com.ipsas.ginf3.gestcabmed.dao.repository;

import org.springframework.data.repository.CrudRepository;

import com.ipsas.ginf3.gestcabmed.dao.entity.Examen;
import org.springframework.stereotype.Repository;

@Repository
public interface ExamenRepository extends CrudRepository<Examen,Integer>{
	Examen findByExamenId(String examenId);
}
