package com.ipsas.ginf3.gestcabmed.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ipsas.ginf3.gestcabmed.service.UtilisateursService;
import com.ipsas.ginf3.gestcabmed.ui.dto.UtilisateurDto;
import com.ipsas.ginf3.gestcabmed.ui.model.LoginRequest;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final UtilisateursService utilisateursService;
    private final Environment env;
    private AuthenticationManager authenticationManager;

    @Autowired
    public AuthenticationFilter(UtilisateursService utilisateursService, Environment env, AuthenticationManager authenticationManager) {
    	//super.setAuthenticationManager(authenticationManager);
    	this.utilisateursService = utilisateursService;
        this.authenticationManager = authenticationManager;
        this.env = env;
        setFilterProcessesUrl("/auth/login");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            LoginRequest creds = new ObjectMapper().readValue(request.getInputStream(), LoginRequest.class);
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getEmail(),
                            creds.getPassword(),
                            new ArrayList<>()
                    )
            );
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        //super.successfulAuthentication(request, response, chain, authResult); //cette ligne est un poison ne la décommenter sous aucun prétext
        String userName = ((User) authResult.getPrincipal()).getUsername();
        UtilisateurDto userDetails = utilisateursService.getUserDetailsByEmail(userName);
        String token = Jwts.builder()
                .setSubject(userDetails.getUtilisateurId())
                .setExpiration(new Date(System.currentTimeMillis() + Long.parseLong(Objects.requireNonNull(env.getProperty("token.expiration_time")))))
                .signWith(SignatureAlgorithm.HS512, env.getProperty("token.secret"))
                .compact();
        response.addHeader("token", token);
        response.addHeader("userId", userDetails.getUtilisateurId());
        response.addHeader("expireAt", Objects.requireNonNull(env.getProperty("token.expiration_time")));
        String res = "{ \"token\" : \""+token+"\", \"expireAt\" : \"864000000\" }";
        response.getWriter().write(res);
    }

}
