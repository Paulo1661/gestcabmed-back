package com.ipsas.ginf3.gestcabmed.ui.controller;

import com.ipsas.ginf3.gestcabmed.service.CertificatMedicalService;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatAptitude;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatAptitudeResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatDispense;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatDispenseResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatRepos;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatReposResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/certificatsMedicaux")
public class CertificatMedicalController {
    private final CertificatMedicalService certificatMedicalServise;
    @Autowired
    public CertificatMedicalController(CertificatMedicalService certificatMedicalServise) {
        this.certificatMedicalServise = certificatMedicalServise;
    }
    @PostMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            path = "aptitude"
    )
    public ResponseEntity<CreateCertificatAptitudeResponse> createCertificatAptitude(@RequestBody  CreateCertificatAptitude createCertificatAptitude) {
        return new ResponseEntity<>(certificatMedicalServise.createCertificatAptitude(createCertificatAptitude), HttpStatus.CREATED);
    }
    @PostMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            path = "dispense"
    )
    public ResponseEntity<CreateCertificatDispenseResponse> createCertificatDispense(@RequestBody  CreateCertificatDispense createCertificatDispense) {
        return new ResponseEntity<>(certificatMedicalServise.createCertificatDispense(createCertificatDispense), HttpStatus.CREATED);
    }
    @PostMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            path = "repos"
    )
    public ResponseEntity<CreateCertificatReposResponse> createCertificatRepos(@RequestBody  CreateCertificatRepos createCertificatRepos) {
        return new ResponseEntity<>(certificatMedicalServise.createCertificatRepos(createCertificatRepos), HttpStatus.CREATED);
    }
}
