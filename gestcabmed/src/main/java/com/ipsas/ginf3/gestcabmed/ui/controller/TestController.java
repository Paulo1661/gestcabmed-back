package com.ipsas.ginf3.gestcabmed.ui.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
	@GetMapping(path = "/index")
	public ResponseEntity<String> login() {
		return new ResponseEntity<String>("Fine play" , HttpStatus.ACCEPTED);
	}

}
