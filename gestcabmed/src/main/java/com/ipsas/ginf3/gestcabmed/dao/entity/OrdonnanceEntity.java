package com.ipsas.ginf3.gestcabmed.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
@Entity @Table(name = "ordonnances")
public class OrdonnanceEntity {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ordonnance_id")
    private long id;
    @OneToMany(
            mappedBy = "ordonnance"
            //orphanRemoval = true
    )
    private Set<DetailOrdonnance> medicaments = new HashSet<>();
    private Date date_prescription;
    @Column(name = "ordonnance_id_public", unique = true, nullable = true)
    private String ordonnanceId;

    @JsonIgnore
    @OneToOne(mappedBy = "ordonnance")
    private Examen examen;
}
