package com.ipsas.ginf3.gestcabmed.dao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ipsas.ginf3.gestcabmed.dao.entity.Secretaire;

@Repository
public interface SecretaireRepository extends CrudRepository<Secretaire, Long> {


}
