package com.ipsas.ginf3.gestcabmed.service;

import com.ipsas.ginf3.gestcabmed.dao.entity.OrdonnanceEntity;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateOrdonnance;

public interface OrdonnanceService {
    OrdonnanceEntity creerOrdonnance(CreateOrdonnance createOrdonnance);
}
