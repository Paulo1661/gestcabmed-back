package com.ipsas.ginf3.gestcabmed.ui.model;

import javax.validation.constraints.NotNull;

import com.ipsas.ginf3.gestcabmed.shared.ExamenComplementaire;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreateExamenComplementaireEntity {
	@NotNull
    private long dossierMedicalId;
	@NotNull
	private ExamenComplementaire examenComplementaire;
	
}
