package com.ipsas.ginf3.gestcabmed.ui.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class CreateLettreConfrereResponse {
	private String emailConfrere;
	private String objet;
	private String contenu;
	private String examenId;
	private String lettreConfrereId;
}
