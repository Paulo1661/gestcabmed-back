package com.ipsas.ginf3.gestcabmed.ui.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MedicamentPrescrit {
    private long medicamentId;
    private short dose;
    private short frequence;
    private short nbrJour;
}
