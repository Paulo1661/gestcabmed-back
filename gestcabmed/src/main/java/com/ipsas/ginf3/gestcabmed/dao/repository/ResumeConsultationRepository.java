package com.ipsas.ginf3.gestcabmed.dao.repository;

import com.ipsas.ginf3.gestcabmed.dao.entity.ResumeConsultationEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResumeConsultationRepository extends CrudRepository<ResumeConsultationEntity,Long> {
}
