package com.ipsas.ginf3.gestcabmed.dao.repository;

import com.ipsas.ginf3.gestcabmed.dao.entity.MedicamentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicamentRepository extends CrudRepository<MedicamentEntity, Long> {
    MedicamentEntity findByNom(String medicamentName);
}
