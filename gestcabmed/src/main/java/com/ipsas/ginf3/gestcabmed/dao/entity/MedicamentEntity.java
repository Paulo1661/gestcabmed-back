package com.ipsas.ginf3.gestcabmed.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
@Entity @Table(name = "medicaments")
public class MedicamentEntity {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "medicament_id")
    private long id;
    @Column(nullable = false, length = 50)
    private String nom;
    private String description;
    @OneToMany(
            mappedBy = "medicament",
            cascade = CascadeType.ALL
            //orphanRemoval = true
    )
    private Set<DetailOrdonnance> ordonnances;
    @Column(name = "medicament_id_public", unique = true)
    private String medicamentId;
}
