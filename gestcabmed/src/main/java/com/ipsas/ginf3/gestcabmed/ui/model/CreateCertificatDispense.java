package com.ipsas.ginf3.gestcabmed.ui.model;

import java.util.Date;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreateCertificatDispense {
	@NotNull
	private Date dateDebut;
	@NotNull
	private Date dateFin;
	@NotNull
	private String examenId;

}
