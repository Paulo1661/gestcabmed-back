package com.ipsas.ginf3.gestcabmed.ui.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class CreateUserModel {
    @NotNull(message="Email must not be null")
    @Email
    private String email;
    private String nom;
    private String prenom;
    @NotNull(message="Password must not be null")
    @Size(max = 20, min = 6)
    private String password;
    private String adresse;
    private String telephone;
    private Date dateNaissance;
    private Date registerDate;
}
