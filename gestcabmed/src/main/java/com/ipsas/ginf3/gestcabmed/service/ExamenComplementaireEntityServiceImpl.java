package com.ipsas.ginf3.gestcabmed.service;

import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ipsas.ginf3.gestcabmed.dao.entity.DossierMedicalEntity;
import com.ipsas.ginf3.gestcabmed.dao.entity.ExamenComplementaireEntity;
import com.ipsas.ginf3.gestcabmed.dao.repository.DossierMedicalRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.ExamenComplementaireRepository;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateExamenComplementaireEntity;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateExamenComplementaireEntityResponse;
@Service
public class ExamenComplementaireEntityServiceImpl implements ExamenComplementaireEntityService{
	private final ExamenComplementaireRepository examenComplementaireRepository;
	private final DossierMedicalRepository dossierMedicalRepository;
	@Autowired
	public ExamenComplementaireEntityServiceImpl(ExamenComplementaireRepository examenComplementaireRepository, DossierMedicalRepository dossierMedicalRepository) {
		this.examenComplementaireRepository = examenComplementaireRepository;
		this.dossierMedicalRepository = dossierMedicalRepository;
	}

	@Override
	public CreateExamenComplementaireEntityResponse createExamenComplementaireEntity(
			CreateExamenComplementaireEntity createExamenComplementaireEntity) {
		ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        ExamenComplementaireEntity examenComplementaireEntity = modelMapper.map(createExamenComplementaireEntity, ExamenComplementaireEntity.class);
        DossierMedicalEntity dossierMedicalEntity= dossierMedicalRepository.findById(createExamenComplementaireEntity.getDossierMedicalId()).get();
        examenComplementaireEntity.setDossierMedical(dossierMedicalEntity);
        examenComplementaireEntity.setExamenComplementaireId(UUID.randomUUID()+"");
        examenComplementaireEntity = examenComplementaireRepository.save(examenComplementaireEntity);
        CreateExamenComplementaireEntityResponse examencomplementaireEntityResponse = modelMapper.map(examenComplementaireEntity, CreateExamenComplementaireEntityResponse.class);
        examencomplementaireEntityResponse.setDossierMedicalId(dossierMedicalEntity.getId());
        return examencomplementaireEntityResponse;
	}

	@Override
	public CreateExamenComplementaireEntityResponse findExamenComplementaireEntity(
			CreateExamenComplementaireEntity createExamenComplementaireEntity) {
		return null;
	}

}
