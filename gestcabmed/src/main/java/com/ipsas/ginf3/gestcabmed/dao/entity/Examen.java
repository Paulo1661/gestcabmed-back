package com.ipsas.ginf3.gestcabmed.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import javax.persistence.*;

@Entity @Data
@AllArgsConstructor @NoArgsConstructor
public class Examen {
	@Id
	@Column(name = "id")
	private int id;
	@OneToOne
	@JoinColumn(name = "CertificatMedical_id")
	private CertificatMedical certificatMedical;
	@OneToOne
	private OrdonnanceEntity ordonnance;
	@OneToOne
	private ExamenComplementaireEntity examenComplementaire;
	@OneToOne
	private ResumeConsultationEntity resumeConsultation;
	@OneToOne
	private LettreConfrereEntity lettresConfrere;
	@Column(name = "examen_id_public")
	private String examenId;
	private Date dateExamen;
	@ManyToOne
	private DossierMedicalEntity dossierMedical;
}
