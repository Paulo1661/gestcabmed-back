package com.ipsas.ginf3.gestcabmed.service;

import java.util.List;

import com.ipsas.ginf3.gestcabmed.ui.model.CreateRdv;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateRdvResponse;

public interface RdvService {
	CreateRdvResponse creerRdv(CreateRdv createRdv);
	CreateRdvResponse updateRdv(CreateRdvResponse createRdvResponse);
	boolean deleteRdv(CreateRdvResponse createRdvResponse);

	List<CreateRdvResponse> getListRdv(String patientId);
	List<CreateRdvResponse> getListRdvDemand();
	List<CreateRdvResponse> getListRdvConfirmed();

	
}
