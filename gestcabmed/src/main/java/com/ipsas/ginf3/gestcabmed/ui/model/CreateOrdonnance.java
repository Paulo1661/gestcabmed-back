package com.ipsas.ginf3.gestcabmed.ui.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreateOrdonnance {
    private List<MedicamentPrescrit> medicaments;
    private Date date_prescription;
    private String examenId;
}
