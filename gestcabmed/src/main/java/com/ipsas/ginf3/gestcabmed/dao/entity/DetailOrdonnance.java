package com.ipsas.ginf3.gestcabmed.dao.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "detail_ordonnance")
public class DetailOrdonnance {
    @EmbeddedId
    private DetailOrdonnanceId id;
    @ManyToOne
    @JsonIgnore
    private OrdonnanceEntity ordonnance;
    @ManyToOne
    private MedicamentEntity medicament;
    //@Column(nullable = false, length = 3)
    private float dose;
    private short frequence;
    private short nbrJour;
}
