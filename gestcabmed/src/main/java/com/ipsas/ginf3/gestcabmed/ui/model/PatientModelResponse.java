package com.ipsas.ginf3.gestcabmed.ui.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PatientModelResponse {
	private String nom;
	private String prenom;
	private String telephone;
	private String adresse;
	private Date prochainRdv;
	private Date lastRdv;
	private Date registerDate;
	private char sexe;
	private int codeCNAM;
	private int validite;
	private String patientId;
}
