package com.ipsas.ginf3.gestcabmed.ui.controller;

import com.ipsas.ginf3.gestcabmed.dao.entity.OrdonnanceEntity;
import com.ipsas.ginf3.gestcabmed.service.OrdonnanceService;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateOrdonnance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ordonnances")
public class OrdonnanceController {

    private final OrdonnanceService ordonnanceService;
    @Autowired
    public OrdonnanceController(OrdonnanceService ordonnanceService) {
        this.ordonnanceService = ordonnanceService;
    }

    @PostMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public ResponseEntity<OrdonnanceEntity> createOrdonnance(@RequestBody CreateOrdonnance createOrdonnance) {

        return new ResponseEntity<>(ordonnanceService.creerOrdonnance(createOrdonnance), HttpStatus.OK);
    }
}
