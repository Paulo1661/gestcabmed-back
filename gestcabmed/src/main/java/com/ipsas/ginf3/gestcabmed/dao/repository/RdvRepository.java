package com.ipsas.ginf3.gestcabmed.dao.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ipsas.ginf3.gestcabmed.dao.entity.Patient;
import com.ipsas.ginf3.gestcabmed.dao.entity.Rdv;
@Repository
public interface RdvRepository extends CrudRepository<Rdv, Long> {
	Rdv findByRdvId(String rdvId);
	List<Rdv> findByPatient(Patient patient);
	List<Rdv> findByEtat(String etat);
}
