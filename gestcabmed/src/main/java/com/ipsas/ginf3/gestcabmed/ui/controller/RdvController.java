package com.ipsas.ginf3.gestcabmed.ui.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ipsas.ginf3.gestcabmed.service.RdvService;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateRdv;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateRdvResponse;

@RestController
@RequestMapping("/rdv")
public class RdvController {
	private final RdvService rdvService;
    @Autowired
    public RdvController(RdvService rdvService) {
        this.rdvService = rdvService;
    }

    @PostMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public ResponseEntity<CreateRdvResponse> createRdv(@RequestBody CreateRdv createRdv) {

        return new ResponseEntity<>(rdvService.creerRdv(createRdv), HttpStatus.OK);
    }
    @PutMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public ResponseEntity<CreateRdvResponse> updateRdv(@RequestBody CreateRdvResponse createRdvResponse) {	
    	return new ResponseEntity<CreateRdvResponse>(rdvService.updateRdv(createRdvResponse), HttpStatus.ACCEPTED);
    }
    @DeleteMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public ResponseEntity<Boolean> deleteRdv(@RequestBody CreateRdvResponse createRdvResponse) {	
    	return new ResponseEntity<Boolean>(rdvService.deleteRdv(createRdvResponse), HttpStatus.OK);
    }
    @GetMapping(path = "/{patientId}")
    public ResponseEntity<List<CreateRdvResponse>> getRdvs(@PathVariable String patientId) {
    	return new ResponseEntity<List<CreateRdvResponse>>(rdvService.getListRdv(patientId),HttpStatus.OK);
    }
    
    @GetMapping(path = "/Demand")
    public ResponseEntity<List<CreateRdvResponse>> getRdvsDemand() {
    	return new ResponseEntity<List<CreateRdvResponse>>(rdvService.getListRdvDemand(),HttpStatus.OK);
    }
    @GetMapping(path = "/Confirmed")
    public ResponseEntity<List<CreateRdvResponse>> getRdvsConfirmed() {
    	return new ResponseEntity<List<CreateRdvResponse>>(rdvService.getListRdvConfirmed(),HttpStatus.OK);
    }
}
