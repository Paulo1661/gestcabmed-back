package com.ipsas.ginf3.gestcabmed.service;

import java.util.List;

import com.ipsas.ginf3.gestcabmed.ui.model.CreatePatient;
import com.ipsas.ginf3.gestcabmed.ui.model.CreatePatientResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.PatientModelResponse;

public interface PatientService {
	CreatePatientResponse creerPatient(CreatePatient createPatient);
	List<PatientModelResponse> getAllPatients();
	
}
