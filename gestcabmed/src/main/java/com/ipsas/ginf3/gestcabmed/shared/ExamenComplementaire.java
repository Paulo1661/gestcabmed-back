package com.ipsas.ginf3.gestcabmed.shared;

public enum ExamenComplementaire {
    BilanBiologique,
    BilanRadiologique
}
