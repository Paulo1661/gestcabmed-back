package com.ipsas.ginf3.gestcabmed.dao.entity;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data @AllArgsConstructor @NoArgsConstructor @ToString

@Entity
public class Medecin extends Personne{
	@Id
	private long id;
	@OneToOne
	private UtilisateurEntity utilisateurEntity;
}
