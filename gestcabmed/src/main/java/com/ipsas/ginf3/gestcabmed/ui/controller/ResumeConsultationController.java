package com.ipsas.ginf3.gestcabmed.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ipsas.ginf3.gestcabmed.service.ResumeConsultationService;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateResumeConsultationEntity;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateResumeConsultationEntityResponse;


@RestController
@RequestMapping("/resumeConsultation")
public class ResumeConsultationController {
    private final ResumeConsultationService resumeConsultationService;
    @Autowired
    public ResumeConsultationController(ResumeConsultationService resumeConsultationService) {
		this.resumeConsultationService = resumeConsultationService;
	}
    @PostMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public ResponseEntity<CreateResumeConsultationEntityResponse> createResumeConsultation(@RequestBody CreateResumeConsultationEntity createResumeConsultationEntity) {

        return new ResponseEntity<>(resumeConsultationService.createResumeConsultation(createResumeConsultationEntity), HttpStatus.OK);
    }
	
}
