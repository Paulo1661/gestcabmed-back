package com.ipsas.ginf3.gestcabmed.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ipsas.ginf3.gestcabmed.service.MedicamentEntityService;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateMedicamentEntity;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateMedicamentEntityResponse;

@RestController
@RequestMapping("/medicament")
public class MedicamentController {
    private final MedicamentEntityService medicamentEntityService;
    @Autowired
    public MedicamentController(MedicamentEntityService medicamentEntityService) {
        this.medicamentEntityService = medicamentEntityService;
    }

    @PostMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public ResponseEntity<CreateMedicamentEntityResponse> createMedicament(@RequestBody CreateMedicamentEntity createMedicamentEntity) {

        return new ResponseEntity<>(medicamentEntityService.createMedicamenEntity(createMedicamentEntity), HttpStatus.OK);
    }
}
