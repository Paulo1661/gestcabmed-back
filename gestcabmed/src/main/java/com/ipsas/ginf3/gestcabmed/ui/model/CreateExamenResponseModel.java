package com.ipsas.ginf3.gestcabmed.ui.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreateExamenResponseModel {
	private Date dateExamen;
	private String dossierMedicalId;
	private String examenId;
}
