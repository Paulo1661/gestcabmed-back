package com.ipsas.ginf3.gestcabmed.ui.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class LoginRequest {
    @Email
    @NotNull(message = "email cannot be null")
    private String email;
    @NotNull(message = "password cannot be null")
    @Size(min = 6, max = 20, message = "invalid password size. min=4 max=16")
    private String password;
}
