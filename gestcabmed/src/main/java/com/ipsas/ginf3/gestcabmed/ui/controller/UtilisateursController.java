package com.ipsas.ginf3.gestcabmed.ui.controller;

import com.ipsas.ginf3.gestcabmed.service.UtilisateursService;
import com.ipsas.ginf3.gestcabmed.ui.dto.UtilisateurDto;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateUserModel;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateUserResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.PatientModelResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.UpdateUserModel;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/utilisateurs")
public class UtilisateursController {
    private final UtilisateursService utilisateursService;

    @Autowired
    public UtilisateursController(UtilisateursService utilisateursService) {
        this.utilisateursService = utilisateursService;
    }

    @PostMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public ResponseEntity<CreateUserResponse> createUtilisateur(@RequestBody CreateUserModel createUserModel) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        UtilisateurDto utilisateurDto = modelMapper.map(createUserModel,UtilisateurDto.class);
        utilisateurDto = utilisateursService.createUtilisateur(utilisateurDto);
        CreateUserResponse createUserResponse = modelMapper.map(utilisateurDto, CreateUserResponse.class);

        return new ResponseEntity<>(createUserResponse, HttpStatus.CREATED);
    }
    
    @PutMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public ResponseEntity<CreateUserResponse> updateUtilisateur(@RequestBody UpdateUserModel updateUserModel) {	
    	return new ResponseEntity<CreateUserResponse>(utilisateursService.updateUtilisateur(updateUserModel), HttpStatus.ACCEPTED);
    }
    
    @GetMapping
    public ResponseEntity<List<CreateUserResponse>> getAllUtilisateurs() {
    	
    	return new ResponseEntity<List<CreateUserResponse>>(utilisateursService.getAllUtilisateurs(), HttpStatus.OK);
    }
}
