package com.ipsas.ginf3.gestcabmed.service;

import com.ipsas.ginf3.gestcabmed.dao.entity.CertificatAptitudeEntity;
import com.ipsas.ginf3.gestcabmed.dao.entity.CertificatDispenseEntity;
import com.ipsas.ginf3.gestcabmed.dao.entity.CertificatReposEntity;
import com.ipsas.ginf3.gestcabmed.dao.entity.DossierMedicalEntity;
import com.ipsas.ginf3.gestcabmed.dao.repository.CertificatAptitudeRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.CertificatDispenseRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.CertificatReposRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.DossierMedicalRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.ExamenRepository;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatAptitude;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatAptitudeResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatDispense;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatDispenseResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatRepos;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateCertificatReposResponse;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CertificatMedicalServiceImpl implements CertificatMedicalService {
    private final CertificatAptitudeRepository certificatAptitudeRepository;
    private final CertificatDispenseRepository certificatDispenseRepository;
    private final CertificatReposRepository certificatReposRepository;
    private final ExamenRepository examenRepository;

    @Autowired
    public CertificatMedicalServiceImpl(CertificatAptitudeRepository certificatAptitudeRepository, DossierMedicalRepository dossierMedicalRepository, CertificatDispenseRepository certificatDispenseRepository, CertificatReposRepository certificatReposRepository, ExamenRepository examenRepository) {
        this.certificatAptitudeRepository = certificatAptitudeRepository;
		this.certificatDispenseRepository = certificatDispenseRepository;
		this.certificatReposRepository = certificatReposRepository;
		this.examenRepository = examenRepository;
    }

    @Override
    public CreateCertificatAptitudeResponse createCertificatAptitude(CreateCertificatAptitude createCertificatAptitude) {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        CertificatAptitudeEntity certificatAptitudeEntity = modelMapper.map(createCertificatAptitude, CertificatAptitudeEntity.class);
        
        certificatAptitudeEntity.setCertificatAptitudeId(UUID.randomUUID().toString());
        certificatAptitudeEntity.setExamen(examenRepository.findByExamenId(createCertificatAptitude.getExamenId()));
        
        certificatAptitudeEntity = certificatAptitudeRepository.save(certificatAptitudeEntity);
        
        CreateCertificatAptitudeResponse certificatAptitudeResponse = modelMapper.map(certificatAptitudeEntity, CreateCertificatAptitudeResponse.class);
        
        return certificatAptitudeResponse;
    }

	@Override
	public CreateCertificatDispenseResponse createCertificatDispense(CreateCertificatDispense createCertificatDispense) {
		ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        CertificatDispenseEntity certificatDispenseEntity = modelMapper.map(createCertificatDispense, CertificatDispenseEntity.class);
        
        certificatDispenseEntity.setExamen(examenRepository.findByExamenId(createCertificatDispense.getExamenId()));
        certificatDispenseEntity.setCertificatDispenseId(UUID.randomUUID().toString());
        
        certificatDispenseEntity = certificatDispenseRepository.save(certificatDispenseEntity);
        
        CreateCertificatDispenseResponse certificatDispenseResponse = modelMapper.map(certificatDispenseEntity, CreateCertificatDispenseResponse.class);
        
        return certificatDispenseResponse;
	}

	@Override
	public CreateCertificatReposResponse createCertificatRepos(CreateCertificatRepos createCertificatRepos) {
		ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        CertificatReposEntity certificatReposEntity = modelMapper.map(createCertificatRepos, CertificatReposEntity.class);
        
        certificatReposEntity.setExamen(examenRepository.findByExamenId(createCertificatRepos.getExamenId()));
        certificatReposEntity.setCertificatReposId(UUID.randomUUID().toString());
        certificatReposEntity = certificatReposRepository.save(certificatReposEntity);
        CreateCertificatReposResponse certificatReposResponse = modelMapper.map(certificatReposEntity, CreateCertificatReposResponse.class);
        
        return certificatReposResponse;
	}
}
