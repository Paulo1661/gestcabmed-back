package com.ipsas.ginf3.gestcabmed.ui.model;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Data @AllArgsConstructor @NoArgsConstructor @ToString

public class UpdateUserModel {
    private String email;
    private String nom;
    private String prenom;
    private String adresse;
    private String telephone;
    private Date dateNaissance;
    private String utilisateurId;
}
