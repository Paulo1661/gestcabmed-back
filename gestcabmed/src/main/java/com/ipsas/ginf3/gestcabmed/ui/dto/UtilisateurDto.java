package com.ipsas.ginf3.gestcabmed.ui.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UtilisateurDto implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String email;
    private String nom;
    private String prenom;
    private String password;
    private String adresse;
    private String telephone;
    private Date dateNaissance;
    private String utilisateurId;
    private String encryptedPassword;
    private Date registerDate;
}
