package com.ipsas.ginf3.gestcabmed.dao.entity;

import com.ipsas.ginf3.gestcabmed.shared.ExamenComplementaire;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
@Entity @Table(name = "examens_complementaire_patient")
public class ExamenComplementaireEntity {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "examen_complementaire_id")
    private long id;
    private ExamenComplementaire examenComplementaire;
    @Column(name = "examen_complementaire_id_public", unique = true)
    private String examenComplementaireId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dossier_medical_id")
    private DossierMedicalEntity dossierMedical;

    @OneToOne(mappedBy = "examenComplementaire")
    private Examen examen;

}
