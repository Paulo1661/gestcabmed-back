package com.ipsas.ginf3.gestcabmed.dao.repository;

import com.ipsas.ginf3.gestcabmed.dao.entity.DossierMedicalEntity;
import org.springframework.data.repository.CrudRepository;

public interface DossierMedicalRepository extends CrudRepository<DossierMedicalEntity,Long> {
	DossierMedicalEntity findByDossierMedicalId(String dossierMedicalId);
}
