package com.ipsas.ginf3.gestcabmed.service;

import com.ipsas.ginf3.gestcabmed.dao.entity.Patient;
import com.ipsas.ginf3.gestcabmed.dao.entity.UtilisateurEntity;
import com.ipsas.ginf3.gestcabmed.dao.repository.UtilisateurRepository;
import com.ipsas.ginf3.gestcabmed.ui.dto.UtilisateurDto;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateUserResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.PatientModelResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.UpdateUserModel;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class UtilisateursServiceImpl implements UtilisateursService {
    private final UtilisateurRepository utilisateurRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UtilisateursServiceImpl(UtilisateurRepository utilisateurRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.utilisateurRepository = utilisateurRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public UtilisateurDto createUtilisateur(UtilisateurDto utilisateur) {
        utilisateur.setUtilisateurId(UUID.randomUUID().toString());
        utilisateur.setEncryptedPassword(bCryptPasswordEncoder.encode(utilisateur.getPassword()));
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        UtilisateurEntity utilisateurEntity = modelMapper.map(utilisateur, UtilisateurEntity.class);
        utilisateurEntity = utilisateurRepository.save(utilisateurEntity);
        UtilisateurDto utilisateurDto = modelMapper.map(utilisateurEntity, UtilisateurDto.class);
        return utilisateurDto;
    }

    @Override
    public UtilisateurDto getUserDetailsByEmail(String userName) {
        UtilisateurEntity utilisateurEntity = utilisateurRepository.findByEmail(userName);
        if (utilisateurEntity == null) throw new UsernameNotFoundException(userName);
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return modelMapper.map(utilisateurEntity, UtilisateurDto.class);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UtilisateurEntity utilisateurEntity = utilisateurRepository.findByEmail(s);
        if (utilisateurEntity == null) throw new UsernameNotFoundException(s);
        User u = new User(utilisateurEntity.getEmail(),utilisateurEntity.getEncryptedPassword(),true,true,true,true,new ArrayList<>());
        return u;
    }

	@Override
	public CreateUserResponse updateUtilisateur(UpdateUserModel updateUserModel) {
		UtilisateurEntity u = utilisateurRepository.findByUtilisateurId(updateUserModel.getUtilisateurId());
		if (u == null) 
			return null;
		ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        UtilisateurEntity user = modelMapper.map(updateUserModel, UtilisateurEntity.class);
        user.setId(u.getId());
        user.setEncryptedPassword(u.getEncryptedPassword());
        user = utilisateurRepository.save(user);
        CreateUserResponse response = modelMapper.map(user, CreateUserResponse.class);
		return response;
	}

	@Override
	public List<CreateUserResponse> getAllUtilisateurs() {
		ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
		List<CreateUserResponse> utilisateurs = new ArrayList<CreateUserResponse>();
		for(UtilisateurEntity u : utilisateurRepository.findAll()) {
			CreateUserResponse utilisateurResponse = modelMapper.map(u, CreateUserResponse.class);
			utilisateurs.add(utilisateurResponse);
		}
		return utilisateurs;
	}
}
