package com.ipsas.ginf3.gestcabmed.service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ipsas.ginf3.gestcabmed.dao.entity.Patient;
import com.ipsas.ginf3.gestcabmed.dao.entity.Rdv;
import com.ipsas.ginf3.gestcabmed.dao.repository.PatientRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.RdvRepository;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateRdv;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateRdvResponse;
@Service
public class RdvServiceImpl implements RdvService {
	private final RdvRepository rdvRepository;
	private final PatientRepository patientRepository;
	@Autowired
	private ModelMapper modelMapper; 
	
	@Autowired
	public RdvServiceImpl(RdvRepository rdvRepository, PatientRepository patientRepository) {
		super();
		this.rdvRepository = rdvRepository;
		this.patientRepository = patientRepository;
	}

	@Override
	public CreateRdvResponse creerRdv(CreateRdv createRdv) {
        Rdv rdv = modelMapper.map(createRdv, Rdv.class);
        rdv.setRdvId(UUID.randomUUID().toString());
        rdv = rdvRepository.save(rdv);
        CreateRdvResponse rdvResponse = modelMapper.map(rdv, CreateRdvResponse.class);
        System.out.println(rdvResponse.getDate());
        return rdvResponse;
	}

	@Override
	public List<CreateRdvResponse> getListRdv(String patientId) {
		Patient p = patientRepository.findByPatientId(patientId);
		List<Rdv> rdvs =  rdvRepository.findByPatient(p);
		List<CreateRdvResponse> rdvResponse = new ArrayList<CreateRdvResponse>();
		for(Rdv r : rdvs) {
			rdvResponse.add(modelMapper.map(r, CreateRdvResponse.class));
		}
		
		return  rdvResponse;		
	}

	@Override
	public List<CreateRdvResponse> getListRdvDemand() {
		List<Rdv> rdvs =  rdvRepository.findByEtat("Demand");
		List<CreateRdvResponse> rdvResponse = new ArrayList<CreateRdvResponse>();
		for(Rdv r : rdvs) {
			rdvResponse.add(modelMapper.map(r, CreateRdvResponse.class));
		}
		return rdvResponse;
	}

	@Override
	public List<CreateRdvResponse> getListRdvConfirmed() {
		List<Rdv> rdvs =  rdvRepository.findByEtat("Confirmed");
		List<CreateRdvResponse> rdvResponse = new ArrayList<CreateRdvResponse>();
		for(Rdv r : rdvs) {
			rdvResponse.add(modelMapper.map(r, CreateRdvResponse.class));
		}
		return rdvResponse;
	}

	@Override
	public CreateRdvResponse updateRdv(CreateRdvResponse createRdvResponse) {
		Rdv r = rdvRepository.findByRdvId(createRdvResponse.getRdvId());
		if (r == null) 
			return null;
		r.setDate(createRdvResponse.getDate());
		r.setEtat(createRdvResponse.getEtat());
        r = rdvRepository.save(r);
        CreateRdvResponse response=modelMapper.map(r, CreateRdvResponse.class);
        return response;
	}

	@Override
	public boolean deleteRdv(CreateRdvResponse createRdvResponse) {
		Rdv r = rdvRepository.findByRdvId(createRdvResponse.getRdvId());
		if(r==null)
			return false;
		rdvRepository.delete(r);
		return true;
	}

}
