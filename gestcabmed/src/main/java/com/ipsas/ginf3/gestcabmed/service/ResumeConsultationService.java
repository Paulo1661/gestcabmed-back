package com.ipsas.ginf3.gestcabmed.service;

import com.ipsas.ginf3.gestcabmed.ui.model.CreateResumeConsultationEntity;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateResumeConsultationEntityResponse;

public interface ResumeConsultationService {
	CreateResumeConsultationEntityResponse createResumeConsultation(CreateResumeConsultationEntity createResumeConsultationEntity);

}
