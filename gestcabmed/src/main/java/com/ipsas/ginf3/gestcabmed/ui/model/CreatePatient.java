package com.ipsas.ginf3.gestcabmed.ui.model;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreatePatient {
	@NotNull
	private char sexe;
	@NotNull
	private int codeCNAM;
	@NotNull
	private int validite;
	@NotNull
	private String utilisateurId;
}
