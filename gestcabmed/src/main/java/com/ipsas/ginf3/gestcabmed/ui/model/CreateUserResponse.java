package com.ipsas.ginf3.gestcabmed.ui.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreateUserResponse {
    private String email;
    private String nom;
    private String prenom;
    private String adresse;
    private String telephone;
    private Date dateNaissance;
    private Date registerDate;
    private String utilisateurId;
}
