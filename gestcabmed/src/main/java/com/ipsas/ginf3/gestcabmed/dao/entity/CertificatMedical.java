package com.ipsas.ginf3.gestcabmed.dao.entity;

import lombok.Data;

import javax.persistence.*;

@Entity @Table(name = "certificats_medicaux")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
public abstract class CertificatMedical {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @OneToOne(mappedBy = "certificatMedical")
    private Examen examen;
}
