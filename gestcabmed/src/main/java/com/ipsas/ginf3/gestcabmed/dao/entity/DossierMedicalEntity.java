package com.ipsas.ginf3.gestcabmed.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data @NoArgsConstructor @AllArgsConstructor @ToString
@Entity @Table(name = "dossiers_medicaux")
public class DossierMedicalEntity {
	
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "dossier_medical_id")
    private long id;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "dossierMedical")
    private Set<Examen> list_Examen = new HashSet<>();
    
    @OneToOne(mappedBy = "dossierMedical")
    private Patient patient;
    
    @Column(name = "dossier_medical_id_public", unique = true)
    private String dossierMedicalId;
}
