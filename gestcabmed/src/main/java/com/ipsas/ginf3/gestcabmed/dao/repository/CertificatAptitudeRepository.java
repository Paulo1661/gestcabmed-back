package com.ipsas.ginf3.gestcabmed.dao.repository;

import com.ipsas.ginf3.gestcabmed.dao.entity.CertificatAptitudeEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CertificatAptitudeRepository extends CrudRepository<CertificatAptitudeEntity, Long> {
}
