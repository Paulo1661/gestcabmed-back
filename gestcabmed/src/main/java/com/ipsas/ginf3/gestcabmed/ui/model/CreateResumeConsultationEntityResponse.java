package com.ipsas.ginf3.gestcabmed.ui.model;

import java.util.Date;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data @AllArgsConstructor @NoArgsConstructor @ToString

public class CreateResumeConsultationEntityResponse {
	private Date dateConsultation;
	private String resume;
	private String traitement;
    private long dossierMedicalId;
    private String resumeConsultationId;
}
