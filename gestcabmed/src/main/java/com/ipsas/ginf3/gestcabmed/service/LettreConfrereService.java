package com.ipsas.ginf3.gestcabmed.service;

import com.ipsas.ginf3.gestcabmed.ui.model.CreateLettreConfrereModel;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateLettreConfrereResponse;

public interface LettreConfrereService {
	CreateLettreConfrereResponse createLettreConfrere(CreateLettreConfrereModel lettre);
}
