package com.ipsas.ginf3.gestcabmed.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
@Entity @Table(name = "certificats_dispense")
public class CertificatDispenseEntity extends CertificatMedical {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "certificat_dispense_id")
    private long id;
    @Column(name = "date_debut_dispense", nullable = false)
    private Date dateDebut;
    @Column(name = "date_fin_dispense", nullable = false)
    private Date dateFin;
    @Column(name = "certificat_dispense_id_public", unique = true)
    private String certificatDispenseId;
}
