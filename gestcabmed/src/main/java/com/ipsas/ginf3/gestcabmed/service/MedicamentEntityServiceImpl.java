package com.ipsas.ginf3.gestcabmed.service;



import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ipsas.ginf3.gestcabmed.dao.entity.MedicamentEntity;
import com.ipsas.ginf3.gestcabmed.dao.repository.MedicamentRepository;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateMedicamentEntity;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateMedicamentEntityResponse;
@Service
public class MedicamentEntityServiceImpl implements MedicamentEntityService {
	private final MedicamentRepository medicamentRepository;
	@Autowired
	public MedicamentEntityServiceImpl(MedicamentRepository medicamentRepository) {
		this.medicamentRepository = medicamentRepository;
	}
	@Override
	public CreateMedicamentEntityResponse createMedicamenEntity(CreateMedicamentEntity createMedicamentEntity) {
		ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        MedicamentEntity medicamentEntity = modelMapper.map(createMedicamentEntity, MedicamentEntity.class);
        medicamentEntity.setMedicamentId(UUID.randomUUID()+"");
        medicamentEntity = medicamentRepository.save(medicamentEntity);
        CreateMedicamentEntityResponse medicamentEntityResponse = modelMapper.map(medicamentEntity, CreateMedicamentEntityResponse.class);
        return medicamentEntityResponse;
	}

}
