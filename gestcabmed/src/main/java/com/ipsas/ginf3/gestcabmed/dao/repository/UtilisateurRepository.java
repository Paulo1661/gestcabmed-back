package com.ipsas.ginf3.gestcabmed.dao.repository;

import com.ipsas.ginf3.gestcabmed.dao.entity.UtilisateurEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilisateurRepository extends CrudRepository<UtilisateurEntity, Long> {
    UtilisateurEntity findByEmail(String email);
    UtilisateurEntity findByUtilisateurId(String utilisateurId);
}
