package com.ipsas.ginf3.gestcabmed.ui.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {
    @RequestMapping(method = RequestMethod.GET, path = "/", produces = MediaType.ALL_VALUE)
    public ResponseEntity<String> index() {
        return new ResponseEntity<>("welcome", HttpStatus.OK);
    }
}
