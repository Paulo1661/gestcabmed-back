package com.ipsas.ginf3.gestcabmed.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ipsas.ginf3.gestcabmed.service.ExamenComplementaireEntityService;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateExamenComplementaireEntity;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateExamenComplementaireEntityResponse;

@RestController
@RequestMapping("/examenComplementaire")
public class ExamenComplementaireController {
	private final ExamenComplementaireEntityService examenComplementaireEntityService;
    @Autowired
    public ExamenComplementaireController(ExamenComplementaireEntityService examenComplementaireEntityService) {
        this.examenComplementaireEntityService = examenComplementaireEntityService;
    }

    @PostMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public ResponseEntity<CreateExamenComplementaireEntityResponse> createExamenComplementaire(@RequestBody CreateExamenComplementaireEntity createExamenComplementaireEntity) {

        return new ResponseEntity<>(examenComplementaireEntityService.createExamenComplementaireEntity(createExamenComplementaireEntity), HttpStatus.OK);
    }
}
