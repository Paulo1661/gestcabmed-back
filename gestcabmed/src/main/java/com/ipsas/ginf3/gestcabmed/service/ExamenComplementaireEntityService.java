package com.ipsas.ginf3.gestcabmed.service;

import com.ipsas.ginf3.gestcabmed.ui.model.CreateExamenComplementaireEntity;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateExamenComplementaireEntityResponse;

public interface ExamenComplementaireEntityService {
	CreateExamenComplementaireEntityResponse createExamenComplementaireEntity(CreateExamenComplementaireEntity createExamenComplementaireEntity);
	CreateExamenComplementaireEntityResponse findExamenComplementaireEntity(CreateExamenComplementaireEntity createExamenComplementaireEntity);
}
