package com.ipsas.ginf3.gestcabmed.service;

import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ipsas.ginf3.gestcabmed.dao.entity.DossierMedicalEntity;
import com.ipsas.ginf3.gestcabmed.dao.entity.ResumeConsultationEntity;
import com.ipsas.ginf3.gestcabmed.dao.repository.DossierMedicalRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.ResumeConsultationRepository;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateResumeConsultationEntity;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateResumeConsultationEntityResponse;
@Service
public class ResumeConsultationServiceImpl implements ResumeConsultationService {
	private final ResumeConsultationRepository resumeConsultationRepository;
    private final DossierMedicalRepository dossierMedicalRepository;
    @Autowired
	public ResumeConsultationServiceImpl(ResumeConsultationRepository resumeConsultationRepository, DossierMedicalRepository dossierMedicalRepository) {
		this.resumeConsultationRepository = resumeConsultationRepository;
		this.dossierMedicalRepository = dossierMedicalRepository;
	}
	@Override
	public CreateResumeConsultationEntityResponse createResumeConsultation(
			CreateResumeConsultationEntity createResumeConsultationEntity) {
		ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        ResumeConsultationEntity resumeConsultationEntity = modelMapper.map(createResumeConsultationEntity, ResumeConsultationEntity.class);
        DossierMedicalEntity dossierMedicalEntity= dossierMedicalRepository.findById(createResumeConsultationEntity.getDossierMedicalId()).get();
        //resumeConsultationEntity.setDossierMedical(dossierMedicalEntity);
        resumeConsultationEntity.setResumeConsultationId(UUID.randomUUID()+"");
        resumeConsultationEntity = resumeConsultationRepository.save(resumeConsultationEntity);
        CreateResumeConsultationEntityResponse resumeConsultationEntityResponse = modelMapper.map(resumeConsultationEntity, CreateResumeConsultationEntityResponse.class);
        resumeConsultationEntityResponse.setDossierMedicalId(dossierMedicalEntity.getId());
        return resumeConsultationEntityResponse;
	}

}
