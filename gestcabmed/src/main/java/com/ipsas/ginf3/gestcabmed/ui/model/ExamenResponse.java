package com.ipsas.ginf3.gestcabmed.ui.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Data @AllArgsConstructor @NoArgsConstructor @ToString

public class ExamenResponse {
	private String certificatMedicalId;
	private String ordonnanceId;
	private String examenComplementaireId;
	private String resumeConsultationId;
	private String lettreConfrereId;
	private String examenId;
	private Date dateExamen;
	private String dossierMedicalId;
}
