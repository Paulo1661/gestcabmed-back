package com.ipsas.ginf3.gestcabmed.service;

import com.ipsas.ginf3.gestcabmed.dao.entity.*;
import com.ipsas.ginf3.gestcabmed.dao.repository.DetailOrdonnanceRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.ExamenRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.MedicamentRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.OrdonnanceRepository;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateOrdonnance;
import com.ipsas.ginf3.gestcabmed.ui.model.MedicamentPrescrit;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class OrdonnanceServiceImpl implements OrdonnanceService {

    private final OrdonnanceRepository ordonnanceRepository;
    private final DetailOrdonnanceRepository detailOrdonnanceRepository;
    private final MedicamentRepository medicamentRepository;
    private final ExamenRepository examenRepository;

    @Autowired
    public OrdonnanceServiceImpl(OrdonnanceRepository ordonnanceRepository,
                                 DetailOrdonnanceRepository detailOrdonnanceRepository,
                                 MedicamentRepository medicamentRepository, ExamenRepository examenRepository) {
        this.ordonnanceRepository = ordonnanceRepository;
        this.detailOrdonnanceRepository=detailOrdonnanceRepository;
        this.medicamentRepository=medicamentRepository;
        this.examenRepository = examenRepository;
    }

    @Override
    public OrdonnanceEntity creerOrdonnance(CreateOrdonnance createOrdonnance) {
        ModelMapper modelMapper = new ModelMapper();
        Examen examen = examenRepository.findByExamenId(createOrdonnance.getExamenId());
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        OrdonnanceEntity ordonnanceEntity = modelMapper.map(createOrdonnance, OrdonnanceEntity.class);
        ordonnanceEntity.setExamen(examen);
        ordonnanceEntity = ordonnanceRepository.save(ordonnanceEntity);
        Set<DetailOrdonnance> detailOrdonnances = new HashSet<>();
        for (MedicamentPrescrit m: createOrdonnance.getMedicaments()) {
            DetailOrdonnance d = new DetailOrdonnance();
            DetailOrdonnanceId id = new DetailOrdonnanceId(ordonnanceEntity.getId(), m.getMedicamentId());
            d.setId(id);
            //for (DetailOrdonnance d : detailOrdonnances) {
            d.setDose(m.getDose());
            d.setFrequence(m.getFrequence());
            d.setNbrJour(m.getNbrJour());
                d.setMedicament(medicamentRepository.findById(m.getMedicamentId()).get());
                d.setOrdonnance(ordonnanceEntity);
                d=detailOrdonnanceRepository.save(d);
            detailOrdonnances.add(d);
            //}
        }
        ordonnanceEntity.setMedicaments(detailOrdonnances);
        return ordonnanceEntity;
    }
}
