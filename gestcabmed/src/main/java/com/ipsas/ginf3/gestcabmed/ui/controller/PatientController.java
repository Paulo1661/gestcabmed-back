package com.ipsas.ginf3.gestcabmed.ui.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ipsas.ginf3.gestcabmed.dao.entity.Examen;
import com.ipsas.ginf3.gestcabmed.service.ExamenService;
import com.ipsas.ginf3.gestcabmed.service.PatientService;
import com.ipsas.ginf3.gestcabmed.ui.model.CreatePatient;
import com.ipsas.ginf3.gestcabmed.ui.model.CreatePatientResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.PatientModelResponse;


@RestController
@RequestMapping("/patients")
public class PatientController {
    private final PatientService patientService;
    private final ExamenService examenService;
    @Autowired
    public PatientController(PatientService patientService, ExamenService examenService) {
        this.patientService = patientService;
		this.examenService = examenService;
    }

    @PostMapping(
            consumes = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE }
    )
    public ResponseEntity<CreatePatientResponse> createPatient(@RequestBody CreatePatient createPatient) {
    	CreatePatientResponse resp = patientService.creerPatient(createPatient);
    	if(resp != null)
    		return new ResponseEntity<>(resp, HttpStatus.OK);
        return new ResponseEntity<>(resp, HttpStatus.CONFLICT);
    }
    
    @GetMapping
    public ResponseEntity<List<PatientModelResponse>> getAllPatients() {
    	
    	return new ResponseEntity<List<PatientModelResponse>>(patientService.getAllPatients(), HttpStatus.OK);
    }
    @GetMapping(path = "/{patientId}/examens")
    public ResponseEntity<List<Examen>> getExams(@PathVariable String patientId) {
    	return new ResponseEntity<List<Examen>>(examenService.getListExamen(patientId),HttpStatus.OK);
    }
}
