package com.ipsas.ginf3.gestcabmed.ui.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
public class CreateCertificatReposResponse {
    private short nombreJoursDeRepos;
    private String certificatReposId;
}
