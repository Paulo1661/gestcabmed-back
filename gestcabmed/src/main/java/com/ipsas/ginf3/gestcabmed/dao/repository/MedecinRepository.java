package com.ipsas.ginf3.gestcabmed.dao.repository;

import com.ipsas.ginf3.gestcabmed.dao.entity.MedicamentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ipsas.ginf3.gestcabmed.dao.entity.Medecin;

@Repository
public interface MedecinRepository extends CrudRepository<Medecin, Long> {
}
