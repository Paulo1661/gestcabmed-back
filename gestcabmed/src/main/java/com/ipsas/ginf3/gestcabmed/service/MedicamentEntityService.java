package com.ipsas.ginf3.gestcabmed.service;

import com.ipsas.ginf3.gestcabmed.ui.model.CreateMedicamentEntity;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateMedicamentEntityResponse;

public interface MedicamentEntityService {
	CreateMedicamentEntityResponse createMedicamenEntity(CreateMedicamentEntity createMedicamentEntity);

}
