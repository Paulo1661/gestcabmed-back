package com.ipsas.ginf3.gestcabmed;

import com.ipsas.ginf3.gestcabmed.dao.entity.DossierMedicalEntity;
import com.ipsas.ginf3.gestcabmed.dao.entity.MedicamentEntity;
import com.ipsas.ginf3.gestcabmed.dao.repository.DossierMedicalRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.MedicamentRepository;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class GestcabmedApplication implements CommandLineRunner {

	@Autowired
	public MedicamentRepository medicamentRepository;
	@Autowired
	public DossierMedicalRepository dossierMedicalRepository;
	public static void main(String[] args) {
		SpringApplication.run(GestcabmedApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//medicamentRepository.save(new MedicamentEntity(1,"paracetamol","desc med",null,null));
		//dossierMedicalRepository.save(new DossierMedicalEntity());
	}

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public ModelMapper getModelMaper() {
		ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return modelMapper;
	}
}
