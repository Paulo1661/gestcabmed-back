package com.ipsas.ginf3.gestcabmed.service;

import java.util.List;

import com.ipsas.ginf3.gestcabmed.dao.entity.Examen;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateExamenModel;
import com.ipsas.ginf3.gestcabmed.ui.model.CreateExamenResponseModel;
import com.ipsas.ginf3.gestcabmed.ui.model.ExamenResponse;

public interface ExamenService {
	CreateExamenResponseModel createExamen(CreateExamenModel examen);
	List<Examen> getListExamen(String patientId);
}
