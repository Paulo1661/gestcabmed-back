package com.ipsas.ginf3.gestcabmed.ui.model;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreateResumeConsultationEntity {
	@NotNull
	private Date dateConsultation;
	@NotNull
	private String resume;
	@NotNull
	private String traitement;
	@NotNull
    private long dossierMedicalId;
}
