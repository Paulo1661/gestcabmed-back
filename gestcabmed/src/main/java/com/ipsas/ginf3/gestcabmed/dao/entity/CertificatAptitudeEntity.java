package com.ipsas.ginf3.gestcabmed.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data @AllArgsConstructor @NoArgsConstructor @ToString
@Entity @Table(name = "certificats_aptitude")
public class CertificatAptitudeEntity extends CertificatMedical {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "certificat_aptitude_id")
    private long id;
    @Column(name = "est_apte", nullable = false)
    private boolean estApte;
    @Column(name = "certificat_aptitude_id_public", unique = true)
    private String certificatAptitudeId;
}
