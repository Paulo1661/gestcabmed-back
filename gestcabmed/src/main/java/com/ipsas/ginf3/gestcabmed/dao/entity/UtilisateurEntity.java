package com.ipsas.ginf3.gestcabmed.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "utilisateurs")
public class UtilisateurEntity extends  Personne{
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "utilisateur_id")
    private long id;
    private String email;
    @Column(name = "utilisateur_id_public",unique = true)
    private String utilisateurId;
    private String encryptedPassword;
    @OneToOne(mappedBy = "utilisateurEntity")
    private Patient patient;
    @OneToOne(mappedBy = "utilisateurEntity")
    private Medecin medecin;
    @OneToOne(mappedBy = "utilisateurEntity")
    private Secretaire secretaire;
    private Date registerDate;
}
