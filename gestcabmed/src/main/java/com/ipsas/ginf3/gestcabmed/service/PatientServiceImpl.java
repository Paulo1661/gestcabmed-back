package com.ipsas.ginf3.gestcabmed.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ipsas.ginf3.gestcabmed.dao.entity.DossierMedicalEntity;
import com.ipsas.ginf3.gestcabmed.dao.entity.Patient;
import com.ipsas.ginf3.gestcabmed.dao.entity.UtilisateurEntity;
import com.ipsas.ginf3.gestcabmed.dao.repository.DossierMedicalRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.PatientRepository;
import com.ipsas.ginf3.gestcabmed.dao.repository.UtilisateurRepository;
import com.ipsas.ginf3.gestcabmed.ui.model.CreatePatient;
import com.ipsas.ginf3.gestcabmed.ui.model.CreatePatientResponse;
import com.ipsas.ginf3.gestcabmed.ui.model.PatientModelResponse;
@Service
public class PatientServiceImpl implements PatientService {
	private final PatientRepository patientRepository;
	private final UtilisateurRepository utilisateurRepository;
	private final DossierMedicalRepository dossierMedicalRepository;
	@Autowired
	public PatientServiceImpl(PatientRepository patientRepository, UtilisateurRepository utilisateurRepository, DossierMedicalRepository dossierMedicalRepository) {
		super();
		this.patientRepository = patientRepository;
		this.utilisateurRepository=utilisateurRepository;
		this.dossierMedicalRepository = dossierMedicalRepository;
	}

	@Override
	public CreatePatientResponse creerPatient(CreatePatient createPatient) {
		Patient p = patientRepository.findByUtilisateurEntity(utilisateurRepository.findByUtilisateurId(createPatient.getUtilisateurId()));
		if(p == null) {
			ModelMapper modelMapper = new ModelMapper();
	        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	        
	        Patient patient = modelMapper.map(createPatient, Patient.class);
	        patient.setPatientId(UUID.randomUUID()+"");
	        
	        UtilisateurEntity u = utilisateurRepository.findByUtilisateurId(createPatient.getUtilisateurId());
	        patient.setUtilisateurEntity(u);
	        DossierMedicalEntity dossierMedical= new DossierMedicalEntity();
	        dossierMedical.setDossierMedicalId(UUID.randomUUID().toString());
	        dossierMedicalRepository.save(dossierMedical);
	        
	        patient = patientRepository.save(patient);
	        CreatePatientResponse patientResponse = modelMapper.map(patient, CreatePatientResponse.class);
	        return patientResponse;
		}
		else {
			return null;
		}
		
	}

	@Override
	public List<PatientModelResponse> getAllPatients() {
		ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
		List<PatientModelResponse> patients = new ArrayList<PatientModelResponse>();
		for(Patient p : patientRepository.findAll()) {
			PatientModelResponse patientResponse = modelMapper.map(p, PatientModelResponse.class);
			patientResponse = modelMapper.map(p.getUtilisateurEntity(), PatientModelResponse.class);
			patientResponse.setRegisterDate(p.getUtilisateurEntity().getRegisterDate());
			patientResponse.setProchainRdv(new Date());
			patientResponse.setLastRdv(new Date());
			patients.add(patientResponse);
		}
			
		return patients;
	}

}
